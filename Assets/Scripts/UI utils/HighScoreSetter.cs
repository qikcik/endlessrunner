using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HighScoreSetter : MonoBehaviour
{
    void Start()
    {
        GameObject gameManager = GameObject.FindGameObjectWithTag("GameManager");
        GetComponent<TextMeshProUGUI>().text = $"HIGH: {gameManager?.GetComponent<HighScoreManager>().getHighScore().ToString()}";
    }

}
