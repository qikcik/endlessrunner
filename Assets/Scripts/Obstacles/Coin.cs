using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            PlayerStateController playerStateController = col.gameObject.GetComponent<PlayerStateController>();
            playerStateController?.playerState?.incrementScore();
            Destroy(gameObject);
        }
    }
}
