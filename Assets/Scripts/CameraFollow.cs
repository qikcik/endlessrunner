using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    [SerializeField] public Vector3 OffsetPosition;

    [SerializeField] public float HorizontalChangeProportion = 0.75f;
    [SerializeField] public float RotationChangeProportion = 1f;
    [SerializeField] public float VerticalRotation = 15f;
    [SerializeField] public float SmoothSpeed = 0.250f;

    void LateUpdate()
    {
        Vector3 desiredPosition = new Vector3(target.position.x * HorizontalChangeProportion, target.position.y, target.position.z) + OffsetPosition;
        Vector3 smootchedPositiom = Vector3.Lerp(transform.position, desiredPosition, SmoothSpeed);
        smootchedPositiom.z = desiredPosition.z;
        transform.position = smootchedPositiom;



        float smootchedRotation = Mathf.Lerp(transform.rotation.y, target.position.x * RotationChangeProportion, SmoothSpeed);
        transform.rotation = Quaternion.Euler(VerticalRotation, smootchedRotation, transform.rotation.z);
    }
}