using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float _forwardSpeed = 1f;
    [SerializeField] float  _verticalSpeed = 5f;


    [SerializeField] float _trackOffset = 1f;

    CharacterController _controller;

    bool _isGrounded = true;
    [SerializeField] public float GravityForce = -9.8f;
    [SerializeField] public float JumpForce = 5f;
    float verticalVelocity = 0;

    [SerializeField] public float FlipingTime = 2f;
    float _flipingTimer = 0;

    short _actualTrack = 0;
    short _targetTrack = 0;
    void Start()
    {
        _controller = GetComponent<CharacterController>();
    }

    public void TryJump() 
    {
        if (_isGrounded)
        {
            verticalVelocity = JumpForce;
            _flipingTimer = 0;
        }
    }
    public void TryFlip() 
    {
        if (_flipingTimer == 0)
        {
            if (!_isGrounded) verticalVelocity -= JumpForce;
            _flipingTimer = FlipingTime;
        }
    }
    public void TryMoveLeft()
    {
        if (_targetTrack > -1)
            _targetTrack--;
        // TODO: not allowed target Track move
    }
    public void TryMoveRight()
    {
        if (_targetTrack < 1)
            _targetTrack++;
        // TODO: not allowed target Track move
    }

    public bool GetIsGrounded() => _isGrounded;
    public float GetFlipingTimer() => _flipingTimer;
    void Update()
    {
        Vector3 move = new Vector3(0,0,0);
        move += horizontalMovement() * Time.deltaTime;
        move += forwardMovement() * Time.deltaTime;
        move += verticalMovement() * Time.deltaTime;

        _controller.Move(move);
        FlipingUpdate();
    }

    Vector3 forwardMovement()
    {
        return Vector3.forward * _forwardSpeed;
    }
    Vector3 horizontalMovement()
    {
        float currentPosition = transform.position.x;
        float targetPosition = _targetTrack * _trackOffset;
        float difference = targetPosition - currentPosition;

        if( difference < 0.2 && difference > -0.2)
        {
            _actualTrack = _targetTrack;
            return new Vector3(0,0,0);
        }

        float moveDirection = (difference > 0 ) ? 1 : -1;
        float easyOutModifier = 1 / Mathf.Abs( _trackOffset / difference );
        easyOutModifier = Mathf.Clamp(easyOutModifier, 0.1f, 10f);

        return Vector3.right * moveDirection * _verticalSpeed * easyOutModifier;
        
    }
    Vector3 verticalMovement()
    {
        if (!_isGrounded && _controller.isGrounded) verticalVelocity = GravityForce * Time.deltaTime;
        if (!_isGrounded) verticalVelocity += GravityForce * Time.deltaTime;

        _isGrounded = _controller.isGrounded;
        return  Vector3.up * verticalVelocity;
    }

    void FlipingUpdate()
    {
        _flipingTimer -= Time.deltaTime;
        if (_flipingTimer < 0) _flipingTimer = 0;
    }
}
