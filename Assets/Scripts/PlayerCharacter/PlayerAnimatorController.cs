using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(CharacterController))]
public class PlayerAnimatorController : MonoBehaviour
{

    Animator _animator;
    PlayerMovement _playerMovement;
    CharacterController _controller;

    [SerializeField] Vector3 ColisionCenterIsGrounded;
    [SerializeField] float ColisionHeightIsGrounded;
    [SerializeField] Vector3 ColisionCenterIsNotGrounded;
    [SerializeField] float ColisionHeightIsNotGrounded;

    [SerializeField] Vector3 ColisionCenterIsFliping;
    [SerializeField] float ColisionHeightIsFliping;
    [SerializeField] Vector3 ColisionCenterIsNotFliping;
    [SerializeField] float ColisionHeightIsNotFliping;
    void Start()
    {
        _animator = GetComponent<Animator>();
        _playerMovement = GetComponent<PlayerMovement>();
        _controller = GetComponent<CharacterController>();
    }

    void Update()
    {

        _animator.SetBool("isGrounded", _playerMovement.GetIsGrounded());
        _animator.SetBool("isFliping", _playerMovement.GetFlipingTimer() != 0);
    }

    public void animEvent_JumpFootOver()
    {
        _controller.center = ColisionCenterIsNotGrounded;
        _controller.height = ColisionHeightIsNotGrounded;
    }
    public void animEvent_JumpFootBack()
    {
        _controller.center = ColisionCenterIsGrounded;
        _controller.height = ColisionHeightIsGrounded;
    }

    public void animEvent_FlipStart()
    {
        _controller.center = ColisionCenterIsFliping;
        _controller.height = ColisionHeightIsFliping;
    }
    public void animEvent_FlipEnd()
    {
        _controller.center = ColisionCenterIsNotFliping;
        _controller.height = ColisionHeightIsNotFliping;
    }
}
