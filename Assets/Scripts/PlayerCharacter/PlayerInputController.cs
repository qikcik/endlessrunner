using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
public class PlayerInputController : MonoBehaviour
{
    void Start()
    {
        GameObject deviceManager = GameObject.FindWithTag("DeviceManager");
        GestureDetection gestureDetection = deviceManager?.GetComponent<GestureDetection>();

        PlayerMovement playerMovement = GetComponent<PlayerMovement>();

        gestureDetection.OnGesture += (Gesture gesture) =>
        {
            switch (gesture)
            {
                case Gesture.slideUp:
                    playerMovement.TryJump();
                    break;
                case Gesture.slideDown:
                    playerMovement.TryFlip();
                    break;
                case Gesture.slideRight:
                    playerMovement.TryMoveRight();
                    break;
                case Gesture.slideLeft:
                    playerMovement.TryMoveLeft();
                    break;
            }
        };
    }

    private object FindWithTag(string v)
    {
        throw new NotImplementedException();
    }
}
