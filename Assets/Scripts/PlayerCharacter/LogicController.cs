using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LogicController : MonoBehaviour
{
    public UnityEvent<PlayerState> onHitted;

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        PlayerState myPlayerState = GetComponent<PlayerStateController>().playerState;

        if (hit.gameObject.tag == "Obstacle")
        {
            onHitted?.Invoke(myPlayerState);
            GetComponent<Animator>().enabled = false;
            GetComponent<PlayerInputController>().enabled = false;
            GetComponent<PlayerMovement>().enabled = false;
        }
    }
}
