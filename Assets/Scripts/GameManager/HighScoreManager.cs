using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScoreManager : MonoBehaviour
{
    public void handeEndAttempt(PlayerState state)
    {
        if (state.getScore() > PlayerPrefs.GetInt("highScore"))
            PlayerPrefs.SetInt("highScore", state.getScore());
    }

    public int getHighScore() => PlayerPrefs.GetInt("highScore");
}
