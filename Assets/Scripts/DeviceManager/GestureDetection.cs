using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public enum Gesture
{
    slideUp,
    slideDown,
    slideLeft,
    slideRight,
}
public class GestureDetection : MonoBehaviour
{
    public Action<Gesture> OnGesture;
    [SerializeField] public float Deadzone = 0.2f;

    Vector2 firstTouchPoint;
    bool _isCapturing = false;

    void Update()
    {
        #if UNITY_EDITOR
            emulateFromKeyboard();
        #endif

        if (Input.touchCount == 0)
            return;

        Touch touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Began)
        {
            firstTouchPoint = touch.position;
            _isCapturing = true;
        }

        if (_isCapturing)
        {
            Vector2 moveVector = firstTouchPoint - touch.position;

            float lowerScreenResolutionDimension = Screen.currentResolution.height < Screen.currentResolution.width ?
                                                    Screen.currentResolution.height : Screen.currentResolution.width;
            float lowerScreenResolutionDimensionSqrt = lowerScreenResolutionDimension * lowerScreenResolutionDimension;

            if (moveVector.SqrMagnitude() > Deadzone* lowerScreenResolutionDimensionSqrt)
            {
                OnGesture?.Invoke(recognizeGesture(moveVector));
                _isCapturing = false;
            }
        }
}

    Gesture recognizeGesture(Vector2 moveVector)
    {
        if (Mathf.Abs(moveVector.x) > Mathf.Abs(moveVector.y))
        {
            if (moveVector.x < 0)
                return Gesture.slideRight;
            else
                return Gesture.slideLeft;
        }
        else
        {
            if (moveVector.y < 0)
                return Gesture.slideUp;
            else
                return Gesture.slideDown;
        }
    }

    #if UNITY_EDITOR
        void emulateFromKeyboard()
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
                OnGesture?.Invoke(Gesture.slideDown);
            if (Input.GetKeyDown(KeyCode.UpArrow))
                OnGesture?.Invoke(Gesture.slideUp);
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                OnGesture?.Invoke(Gesture.slideLeft);
            if (Input.GetKeyDown(KeyCode.RightArrow))
                OnGesture?.Invoke(Gesture.slideRight);
        }
    #endif

}
