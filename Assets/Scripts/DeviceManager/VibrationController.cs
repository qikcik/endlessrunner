using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibrationController : MonoBehaviour
{
    public bool active = true;

    public void Vibrate()
    {
        if(active)
            Handheld.Vibrate();
    }
    // Update is called once per frame
    void Update()
    {
    }
}
