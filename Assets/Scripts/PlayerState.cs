using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerState : MonoBehaviour
{

    private int score = 0;
    public UnityEvent<int> onScoreChanged;

    public int getScore() => score;

    public void incrementScore()
    {
        score++;
        onScoreChanged?.Invoke(score);
    }

}
